"""Module containing functionalities to store run parameters.

Class:
    ParamParse: Take as input a file containing the parameters
    and stores them in its attributes.
"""
import logging
from pathlib import Path

LOG = logging.getLogger(__name__)


class ParamParse:
    """Class holding the parameters of the run.

    Args:
        param_file: Path to file with parameter values.

    Attributes:
        param_file: File with parameter values.
        transcripts_file: File with transcript abundances.
        genome_ref_file: Reference genome file.
        annotations_file: Transcripts annotations.
        output_path: Output folder.
        n_reads: Number of reads to be simulated.
        n_cells: Number of cells to be simulated.
        rna_avg_length: average RNA fragment length.
        rna_sd_length: RNA fragment length standard deviation.
        read_length: Read length.
        intron_rate: Constant probability of retaining an intron.
        add_poly_a: Boolean option to add a poly A tail.
        poly_a_func: Function to add a poly_a tail.
        primer_seq: Sequence of the primer.
        priming_func: Function that evaluates internal priming.
    """

    def __init__(self, param_file: Path) -> None:
        """Class constructor."""
        self.param_file: Path = Path(param_file)
        with open(param_file) as f:
            LOG.info("Loading parameters...")
            for line in f:
                s = line.split(':')
                if s[0] == 'Csv transcripts file':
                    self.transcripts_file: Path = Path(s[1].strip())
                elif s[0] == 'Reference genome file':
                    self.genome_ref_file: Path = Path(s[1].strip())
                elif s[0] == 'Transcripts annotation file':
                    self.annotations_file: Path = Path(s[1].strip())
                elif s[0] == 'Output folder':
                    self.output_path: Path = Path(s[1].strip())
                elif s[0] == 'Number of reads':
                    self.n_reads: int = int(s[1].strip())
                elif s[0] == 'Number of cells':
                    self.n_cells: int = int(s[1].strip())
                elif s[0] == 'Average RNA fragments length':
                    self.rna_avg: float = float(s[1].strip())
                elif s[0] == 'RNA fragment length standard deviation':
                    self.rna_sd_length: float = float(s[1].strip())
                elif s[0] == 'Reads length':
                    self.read_length: int = int(s[1].strip())
                elif s[0] == 'Intron retaining probability':
                    self.intron_rate: float = float(s[1].strip())
                elif s[0] == 'Add poly A tail':
                    self.add_poly_a: bool = bool(s[1].strip())
                elif s[0] == 'Function to add poly A tail':
                    self.poly_a_func: str = str(s[1].strip())
                elif s[0] == 'Primer sequence':
                    self.primer_seq: str = str(s[1].strip())
                elif s[0] == 'Function to evaluate internal priming':
                    self.priming_func: str = str(s[1].strip())
        LOG.info("Parameters loaded.")
