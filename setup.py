"""Setup the package."""

from setuptools import setup, find_packages
import src


setup(
    name='src',
    author='Zavolan Lab, Biozentrum, University of Basel',
    url='https://git.scicore.unibas.ch/zavolan_group/pipelines/scrna-seq-simulation',
    license='MIT',
    version=src.__version__,
    install_requires=[],
    entrypoints={},
    packages=find_packages()
)
