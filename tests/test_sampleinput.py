"""Tests the transcriptome abundance file input reader."""

import pytest
import pandas as pd

from src.sampleinput import sample_from_input


def test_sampleinput(tmpdir):
    """Tests the output, input file name and separator."""
    sample_from_input(
        input_file='./tests/resources/Transcript2.tsv',
        output_file=tmpdir / 'test1.csv',
        sep='\t',
        n=142958
    )
    t1=pd.read_table(tmpdir / 'test1.csv', header=None, sep=',')
    assert t1[1].sum()==142958
    with pytest.raises(IndexError):
        sample_from_input(input_file='./tests/resources/Transcript2.tsv')
    with pytest.raises(IOError):
        sample_from_input(input_file='file_not_existing.txt')
