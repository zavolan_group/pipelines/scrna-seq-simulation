"""Tests the parameter parser class."""

import pytest
from pathlib import Path
from src import parameter_parser as pp
from src import poly_a

def test_parser():
    """Tests the attributes of the class."""
    par=pp.ParamParse('./tests/resources/Param_test.txt')
    assert par.param_file == Path('./tests/resources/Param_test.txt')
    assert par.transcripts_file == Path('./transcripts.csv')
    assert par.genome_ref_file == Path('./home/ref.ref')
    assert par.annotations_file == Path('./home/annotations.ann')
    assert par.output_path == Path('./home/output')
    assert par.n_reads == 10023
    assert par.n_cells == 34
    assert par.rna_avg == 150
    assert par.rna_sd_length == 10
    assert par.read_length == 100
    assert par.intron_rate == 0.2
    assert par.add_poly_a == bool('TRUE')
    assert par.poly_a_func == 'generate_poly_a'
    assert par.primer_seq == 'ACCTGATCGTACG'
    assert par.priming_func == 'internal_priming'