"""Tests for poly_a module."""

import pytest

from src.poly_a import generate_poly_a


class TestGeneratePolyA():
    """Tests for poly(A) tail generation."""

    def test_passes_default_args(self):
        res = generate_poly_a()
        assert isinstance(res, str)
        assert len(res) == 100

    def test_passes_set_all_args(self):
        res = generate_poly_a(
            length=10,
            weights=(1,0,0,0),
        )
        assert isinstance(res, str)
        assert len(res) == 10
        assert res == len(res) * 'A'

    @pytest.mark.parametrize(
        "length, expected",
        [
            ('a', ValueError),
            (-1, ValueError),
            (0, ValueError),
            (250, ValueError),
        ]
    )
    def test_wrong_length(self, expected, length):
        with pytest.raises(expected):
            generate_poly_a(length=length)

    @pytest.mark.parametrize(
        "weights, expected",
        [
            ((0,0,1), ValueError),
            (('a', 0,0,1), ValueError),
            ((0,0,0,-1), ValueError),
            ((0,0,0,0), ValueError),
        ]
    )
    def test_wrong_weights(self, expected, weights):
        with pytest.raises(expected):
            generate_poly_a(weights=weights)
