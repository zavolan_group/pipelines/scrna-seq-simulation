"""Tests for module in root package."""

from re import match

from src import __version__


def test_version():
    """Assert that version matches semantic versioning format."""
    assert match(r'\d\.\d\.\d', __version__)

